package tulio.ing.arriendaya.utilidades;

import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by CristianAlvarez on 23/09/2017.
 */

public class EnviarImagen {
    private static String TAG = "EnviandoImagenes";

    /**
     * Envia una imagen a un servidor
     * @param ruta es la direccion en File de la imagen a enviar
     * @param nombreImagen el nombre que llegará al servidor
     * @return true: imagen enviada correctamente
     */
    public static boolean subirArchivo(String ruta, String nombreImagen){// metodo
        File file = new File(ruta);
        String tipo_de_contenido = getTipo(file.getPath());// si es img, mp3, video, etc

        String ruta_archivo = file.getAbsolutePath();
        OkHttpClient client = new OkHttpClient();
        RequestBody file_body = RequestBody.create(MediaType.parse(tipo_de_contenido), file);

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", tipo_de_contenido)
                .addFormDataPart("uploaded_file", nombreImagen, file_body)
                .build();

        Request request = new Request.Builder()
                .url(Constantes.BASE_URL_IMGS + Constantes.URL_ENVIAR_IMAGENES)//
                .post(requestBody)
                .build();

        try {
            okhttp3.Response response = client.newCall(request).execute();

            try {
                Log.v(TAG, "respuesta "+response.body().string());
            }catch (NullPointerException e){
                e.printStackTrace();
            }

            if(!response.isSuccessful()){
                Log.v(TAG, "no es exitoso");

                return false;
            }else{
                Log.v(TAG, "es exitoso");

                return true;
            }
        } catch (IOException e) {
            Log.v(TAG,"ErrorAlEjecutar "+e.getClass() +" -> "+ e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    private static String getTipo(String path){// metodo 1.1
        String extencion = MimeTypeMap.getFileExtensionFromUrl(path);

        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extencion);
    }
}
