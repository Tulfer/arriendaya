package tulio.ing.arriendaya.basedatos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tulio.ing.arriendaya.modelos.DatosLogin;

import static tulio.ing.arriendaya.basedatos.DBHelper.*;

public class DBManager {
    private DBHelper dbHelper;
    private SQLiteDatabase db;

    public DBManager(Context contexto){
        dbHelper = new DBHelper(contexto);
    }

    public void insertarDatosLogin(DatosLogin datosLogin){
        db = dbHelper.getWritableDatabase();

        db.insert(TABLA_LOGIN, null, datosLogin.toContentValues());
    }


    public DatosLogin getDatosLogin(){
        db = dbHelper.getReadableDatabase();

        DatosLogin datosLogin = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLA_LOGIN, null, null);

        if(cursor.moveToFirst()){
            datosLogin = new DatosLogin(
                    cursor.getString( cursor.getColumnIndex(USUARIO) ),
                    cursor.getString( cursor.getColumnIndex(ID) ),
                    cursor.getString( cursor.getColumnIndex(NOMBRE) ),
                    cursor.getString( cursor.getColumnIndex(APELLIDO) ),
                    cursor.getString( cursor.getColumnIndex(EMAIL) ),
                    cursor.getString( cursor.getColumnIndex(TELEFONO) ),
                    cursor.getString( cursor.getColumnIndex(DIRECCION) ),
                    cursor.getString( cursor.getColumnIndex(SEXO) )
            );
        }

        cursor.close();

        return datosLogin;
    }

    public void borrarDatosLogin(){
        db = dbHelper.getWritableDatabase();

        db.delete(DBHelper.TABLA_LOGIN, null, null);
    }
}
