package tulio.ing.arriendaya.basedatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "arriendara.db", null, 1);
    }

    public static final String TABLA_LOGIN = "datos_login";

    public static final String ID = "id";
    public static final String USUARIO = "usuario";
    public static final String NOMBRE = "nombre";
    public static final String APELLIDO = "apellido";
    public static final String EMAIL = "email";
    public static final String TELEFONO = "telefono";
    public static final String DIRECCION = "direccion";
    public static final String SEXO = "sexo";

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREAR_TABLA_DATOS_LOGIN = " CREATE TABLE "+TABLA_LOGIN+" (" +
                ID + " INT, " +
                USUARIO + " TEXT, " +
                NOMBRE + " TEXT, " +
                APELLIDO + " TEXT, " +
                EMAIL + " TEXT, " +
                TELEFONO + " TEXT, " +
                DIRECCION + " TEXT, " +
                SEXO + " TEXT);";

        db.execSQL( CREAR_TABLA_DATOS_LOGIN );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
