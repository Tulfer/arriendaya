package tulio.ing.arriendaya.modelos;

import java.io.Serializable;

public class Imagen implements Serializable {
    private int id;
    private String direccion;

    public Imagen(int id, String rutaImg) {
        this.id = id;
        this.direccion = rutaImg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
