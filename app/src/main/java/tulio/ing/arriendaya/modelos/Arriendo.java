package tulio.ing.arriendaya.modelos;

import java.util.ArrayList;

public class Arriendo {
    private int id;
    private String urlImagenPersona;
    private String nombrePersona;
    private String descripcion;
    private ArrayList<String> servicios;
    private ArrayList<String> urlsImagenes;
    private String direccion;
    private String precio;
    private String estado;
    private double latitud, longitud;

    public Arriendo(int id, String urlImagenPersona, String nombrePersona, String descripcion, String estado, String precio, ArrayList<String> servicios, ArrayList<String> urlsImagenes, String direccion, double latitud, double longitud) {
        this.id = id;
        this.urlImagenPersona = urlImagenPersona;
        this.nombrePersona = nombrePersona;
        this.urlsImagenes = urlsImagenes;
        this.descripcion = descripcion;
        this.estado = estado;
        this.precio = precio;
        this.servicios = servicios;
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public ArrayList<String> getUrlsImagenes() {
        return urlsImagenes;
    }

    public void setUrlsImagenes(ArrayList<String> urlsImagenes) {
        this.urlsImagenes = urlsImagenes;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlImagenPersona() {
        return urlImagenPersona;
    }

    public void setUrlImagenPersona(String urlImagenPersona) {
        this.urlImagenPersona = urlImagenPersona;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<String> getServicios() {
        return servicios;
    }

    public void setServicios(ArrayList<String> servicios) {
        this.servicios = servicios;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
}
