package tulio.ing.arriendaya.modelos;

public class NovedadItem {
    private int id;
    private String nombrePersona, direccion, urlImgPersona, urlImg, descripcion;

    public String getUrlImgPersona() {
        return urlImgPersona;
    }

    public NovedadItem(int id, String nombrePersona, String direccion, String urlImgPersona, String urlImg, String descripcion) {
        this.id = id;
        this.nombrePersona = nombrePersona;
        this.direccion = direccion;
        this.urlImgPersona = urlImgPersona;
        this.urlImg = urlImg;
        this.descripcion = descripcion;
    }

    public NovedadItem() {
    }

    public void setUrlImgPersona(String urlImgPersona) {
        this.urlImgPersona = urlImgPersona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
