package tulio.ing.arriendaya.modelos;

import android.content.ContentValues;

import static tulio.ing.arriendaya.basedatos.DBHelper.*;

public class DatosLogin {
    private String usuario;
    private String id;
    private String cedula;
    private String nombre;
    private String apellido;
    private String email;
    private String telefono;
    private String direccion;
    private String sexo;

    public DatosLogin(String id, String usuario, String nombre, String apellido, String email, String telefono, String direccion, String sexo) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.sexo = sexo;
    }

    public ContentValues toContentValues(){
        ContentValues values = new ContentValues();

        values.put(ID, id);
        values.put(USUARIO, usuario);
        values.put(NOMBRE, nombre);
        values.put(APELLIDO, apellido);
        values.put(EMAIL, email);
        values.put(TELEFONO, telefono);
        values.put(DIRECCION, direccion);
        values.put(SEXO, sexo);

        return values;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
