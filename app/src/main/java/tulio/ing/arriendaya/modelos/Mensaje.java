package tulio.ing.arriendaya.modelos;

public class Mensaje {
    private String mensaje;
    private DatosLogin usuario;
    private int idArriendo;

    public void setIdArriendo(int idArriendo) {
        this.idArriendo = idArriendo;
    }

    public int getIdArriendo() {
        return idArriendo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public DatosLogin getUsuario() {
        return usuario;
    }

    public void setUsuario(DatosLogin usuario) {
        this.usuario = usuario;
    }
}
