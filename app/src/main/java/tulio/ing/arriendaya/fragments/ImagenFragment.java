package tulio.ing.arriendaya.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import tulio.ing.arriendaya.R;


public class ImagenFragment extends Fragment {

    public static final String ARG_URL_IMG = "url_img";

    public ImagenFragment() {
        //
    }

    public static ImagenFragment newInstance(String urlImg) {
        ImagenFragment fragment = new ImagenFragment();

        Bundle args = new Bundle();
        args.putString(ARG_URL_IMG, urlImg);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_imagen, container, false);

        String urlImagen = "";
        ImageView imagen = layout.findViewById(R.id.imagen);

        Bundle arg = getArguments();
        if(arg != null && arg.getString(ARG_URL_IMG) != null)
            urlImagen = arg.getString(ARG_URL_IMG);

        Glide.with(getContext())
                .load(urlImagen)
                .placeholder(R.drawable.imagen_en_blanco)
                .into(imagen);


        return layout;
    }


}
