package tulio.ing.arriendaya.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tulio.ing.arriendaya.actividades.DetallesArriendoActivity;
import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.adapters.NovedadesAdapter;
import tulio.ing.arriendaya.modelos.Arriendo;
import tulio.ing.arriendaya.modelos.NovedadItem;
import tulio.ing.arriendaya.servicioweb.ServicioWeb;
import tulio.ing.arriendaya.servicioweb.ServicioWebUtils;

/**
 * Created by Desarrollo1 on 19/05/2018.
 */

public class FragmentNovedades extends Fragment implements NovedadesAdapter.ListenerClick{

    private RecyclerView recyclerNovedades;
    private LinearLayout layoutReintentar;
    private TextView txtAnuncioReintentar;
    private Button btnReintentar;
    private NovedadesAdapter adapterNovedades;
    private ArrayList<Arriendo> novedades;
    private ServicioWeb servicioWeb;
    private LinearLayout layoutCargando;

    @Override

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,

                             Bundle savedInstanceState) {

        View vistaInflada = inflater.inflate(R.layout.fragment_novedades, container, false);

        layoutCargando = vistaInflada.findViewById(R.id.layout_cargando_novedades);
        recyclerNovedades = vistaInflada.findViewById(R.id.recycler_novedades);
        layoutReintentar = vistaInflada.findViewById(R.id.layout_reintentar);
        txtAnuncioReintentar = vistaInflada.findViewById(R.id.txt_anuncio_reintentar);
        btnReintentar = vistaInflada.findViewById(R.id.btn_reintentar_novedades);



        RecyclerView.LayoutManager lmNovedades = new LinearLayoutManager(getActivity());
        recyclerNovedades.setLayoutManager(lmNovedades);

        novedades = new ArrayList<>();

        adapterNovedades = new NovedadesAdapter(novedades, this, getActivity());
        recyclerNovedades.setAdapter(adapterNovedades);

        servicioWeb = ServicioWebUtils.getServicioWeb();

        refreshNovedades();

        btnReintentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshNovedades();
            }
        });

        return vistaInflada;

    }

    private void refreshNovedades(){
        layoutReintentar.setVisibility(View.GONE);
        recyclerNovedades.setVisibility(View.INVISIBLE);
        layoutCargando.setVisibility(View.VISIBLE);

        Call<ArrayList<Arriendo>> arrayListCall = servicioWeb.getNovedades("getNovedades");

        arrayListCall.enqueue(new Callback<ArrayList<Arriendo>>() {
            @Override
            public void onResponse(Call<ArrayList<Arriendo>> call, Response<ArrayList<Arriendo>> response) {
                if(response.isSuccessful()){
                    novedades = response.body();

                    if(novedades != null){
                        recyclerNovedades.setVisibility(View.VISIBLE);

                        adapterNovedades.setNovedades(novedades);

                        if(novedades.isEmpty()){
                            txtAnuncioReintentar.setText("No hay novedades");
                            layoutReintentar.setVisibility(View.VISIBLE);

                        }else{
                            layoutReintentar.setVisibility(View.GONE);
                        }
                    }else{
                        Toast.makeText(getContext(), "Novedades nulas", Toast.LENGTH_SHORT).show();
                    }

                    layoutCargando.setVisibility(View.GONE);

                }else{
                    layoutCargando.setVisibility(View.GONE);

                    layoutReintentar.setVisibility(View.VISIBLE);
                    txtAnuncioReintentar.setText("Error en el servidor");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Arriendo>> call, Throwable t) {
                layoutCargando.setVisibility(View.GONE);

                layoutReintentar.setVisibility(View.VISIBLE);
                txtAnuncioReintentar.setText("No hay conexión suficiente");
            }
        });
    }

    @Override
    public void clickDetalles(Arriendo novedad, int posicion) {
        Intent intent = new Intent(getContext(), DetallesArriendoActivity.class);
        intent.putExtra(DetallesArriendoActivity.ARG_ID_ARRIENTO, novedad.getId());
        startActivity(intent);
    }
    
}
