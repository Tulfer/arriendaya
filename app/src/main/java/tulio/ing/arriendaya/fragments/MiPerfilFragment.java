package tulio.ing.arriendaya.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.actividades.CrearPublicacionActivity;
import tulio.ing.arriendaya.actividades.LoginActivity;
import tulio.ing.arriendaya.basedatos.DBManager;
import tulio.ing.arriendaya.modelos.DatosLogin;

public class MiPerfilFragment extends Fragment implements View.OnClickListener{

    private TextView txtUsuario, txtNombres, txtApellidos, txtGenero, txtEmail,
                    txtTelefono, txtDireccion;
    private TextView btnCerrarSesion, btnIrAlLogin;
    private LinearLayout layoutIrAlLogin;
    private CardView cardDatos;
    private DBManager dbManager;
    private Button btnIrPublicarArriendo;

    public MiPerfilFragment() {
        //
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_mi_perfil, container, false);

        dbManager = new DBManager(getContext());

        txtUsuario = vista.findViewById(R.id.txt_usuario);
        txtNombres = vista.findViewById(R.id.txt_nombres);
        txtApellidos = vista.findViewById(R.id.txt_apellidos);
        txtGenero = vista.findViewById(R.id.txt_genero);
        txtEmail = vista.findViewById(R.id.txt_email);
        txtTelefono = vista.findViewById(R.id.txt_telefono);
        txtDireccion = vista.findViewById(R.id.txt_direccion);
        btnCerrarSesion = vista.findViewById(R.id.btn_cerrar_sesion);
        btnIrAlLogin = vista.findViewById(R.id.btn_ir_al_login);
        layoutIrAlLogin = vista.findViewById(R.id.layout_ir_a_login);
        cardDatos = vista.findViewById(R.id.card_layout_datos);
        btnIrPublicarArriendo = vista.findViewById(R.id.btn_ir_publicar_arriendo);

        definirDiseno();

        return vista;
    }

    @Override
    public void onResume() {
        definirDiseno();
        super.onResume();
    }

    private void definirDiseno(){
        DatosLogin datosLogin = dbManager.getDatosLogin();

        if(datosLogin != null){

            txtUsuario.setText( datosLogin.getUsuario() );
            txtNombres.setText( datosLogin.getNombre() );
            txtApellidos.setText( datosLogin.getApellido() );
            txtGenero.setText( datosLogin.getSexo() );
            txtEmail.setText( datosLogin.getEmail() );
            txtTelefono.setText( datosLogin.getTelefono() );
            txtDireccion.setText( datosLogin.getDireccion() );

            btnCerrarSesion.setOnClickListener(this);
            btnIrPublicarArriendo.setOnClickListener(this);

            layoutIrAlLogin.setVisibility(View.GONE);
            cardDatos.setVisibility(View.VISIBLE);
        }else{
            layoutIrAlLogin.setVisibility(View.VISIBLE);
            cardDatos.setVisibility(View.GONE);

            btnIrAlLogin.setOnClickListener(this);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_ir_al_login:
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_cerrar_sesion:
                dbManager.borrarDatosLogin();
                definirDiseno();
                break;
            case R.id.btn_ir_publicar_arriendo:
                Intent intentCrearPub = new Intent(getContext(), CrearPublicacionActivity.class);
                startActivity(intentCrearPub);
                break;
        }
    }
}
