package tulio.ing.arriendaya.actividades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import tulio.ing.arriendaya.R;

public class DefinirCiudadActivity extends AppCompatActivity {

    private ImageView imgFondo, imgLogo;
    private Button btnBuscar;
    private Spinner spnCiudades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definir_ciudad);

        imgFondo = findViewById(R.id.img_fondo_ciudad);
        imgLogo = findViewById(R.id.img_logo_ciudad);
        btnBuscar = findViewById(R.id.btn_buscar_ciudad);
        spnCiudades = findViewById(R.id.spn_ciudades);

        ArrayList<String> ciudades = new ArrayList<>();
        ciudades.add("Ningun lugar");
        ciudades.add("San Pateste");
        ciudades.add("Etc...");

        ArrayAdapter adapterCiudades = new ArrayAdapter(
                this, android.R.layout.simple_spinner_item, ciudades);

        adapterCiudades.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnCiudades.setAdapter(adapterCiudades);

        Glide.with(this)
                .load("")
                .placeholder(R.drawable.fondo_seleccionar_ciudad)
                .into(imgFondo);

        Glide.with(this)
                .load("")
                .placeholder(R.drawable.house_icon)
                .into(imgLogo);
    }
}
