package tulio.ing.arriendaya.actividades;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kosalgeek.android.photoutil.GalleryPhoto;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.adapters.ImagenAdapter;
import tulio.ing.arriendaya.adapters.TextoAdapter;
import tulio.ing.arriendaya.modelos.Imagen;
import tulio.ing.arriendaya.services.EnviarImagenesIntentService;
import tulio.ing.arriendaya.utilidades.FotoDeCamara;

public class CrearPublicacionActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    private static final int INTENT_CAMARA = 987;
    private static final int INTENT_GALERIA = 678;
    private static final int COD_SET_UBICACION = 836;
    private final int COD_PERMISOS = 345;

    private EditText edtDireccion;
    private RecyclerView recyclerFotos;
    private Button btnAgregarFoto, btnPublicar, btnCambiarUbicacion;
    private EditText edtDescripcion;
    private ProgressBar progressPublicar;
    private TextView txtLabelFotos, txtLayoutProgeso, txtNoHaDePos;
    private RelativeLayout layoutProgreso;
    private GoogleMap mapa;
    private MapFragment fragment;
    private ImagenAdapter adapteFotos;

    private Marker markerPosicion;

    private FotoDeCamara cameraPhoto;
    private GalleryPhoto galleryPhoto;
    private TextView txtLabelServicios;
    private RecyclerView recyclerServicios;
    private EditText edtNuevoServicio;
    private Button btnAgregarServicio;
    private TextoAdapter adapterServicios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_publicacion);
        setToolbar();
        inicializarVistas();
        registrarBroadcastReceiver();
    }

    private void inicializarVistas() {
        edtDireccion = findViewById(R.id.edt_direccion_publicacion);
        recyclerFotos = findViewById(R.id.recycler_fotos_publicacion);
        btnAgregarFoto = findViewById(R.id.btn_agregar_foto);
        edtDescripcion = findViewById(R.id.edt_descripcion_publicacion);
        btnPublicar = findViewById(R.id.btn_publicar);
        progressPublicar = findViewById(R.id.progress_publicar);
        txtLabelFotos = findViewById(R.id.txt_label_fotos);
        layoutProgreso = findViewById(R.id.layout_progreso_envio);
        txtLayoutProgeso = findViewById(R.id.txt_layout_progreso);
        btnCambiarUbicacion = findViewById(R.id.btn_cambiar_ubicacion);
        txtNoHaDePos = findViewById(R.id.txt_no_ha_def_ubicacion);

        fragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_mapa_crear_publ);
        fragment.getMapAsync(this);

        btnPublicar.setOnClickListener(this);
        btnAgregarFoto.setOnClickListener(this);
        btnCambiarUbicacion.setOnClickListener(this);

        RecyclerView.LayoutManager layoutManagerHorizontal = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerFotos.setLayoutManager(layoutManagerHorizontal);
        recyclerFotos.setHasFixedSize(true);
        recyclerFotos.setNestedScrollingEnabled(false);

        // Usamos el adapter de los tecnicos para las fotos tomadas
        adapteFotos = new ImagenAdapter(new ArrayList<Imagen>(), null);
        recyclerFotos.setAdapter(adapteFotos);

        txtLabelServicios = findViewById(R.id.txt_label_servicios);
        recyclerServicios = findViewById(R.id.recycler_servicios);
        edtNuevoServicio = findViewById(R.id.edt_nuevo_servicio);
        btnAgregarServicio = findViewById(R.id.btn_agregar_servicio);

        btnAgregarServicio.setOnClickListener(this);

        RecyclerView.LayoutManager lmServicio = new LinearLayoutManager(this);
        recyclerServicios.setLayoutManager(lmServicio);

        adapterServicios = new TextoAdapter(new ArrayList<String>());
        recyclerServicios.setAdapter(adapterServicios);

        txtLabelServicios.setVisibility(View.VISIBLE);

    }

    private void setToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar_solicitar);

        if(toolbar != null){
            setSupportActionBar(toolbar);

            if(getSupportActionBar() != null){
                getSupportActionBar().setTitle("Solicitud");
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void registrarBroadcastReceiver(){
        /* Registramos el broadcast de servicio de sincronizar con sus respectivas acciones a manejar */
        // Creamos el filtro de intens que va a recibir el BroadCast Receiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(EnviarImagenesIntentService.ACTUALIZAR_PROGESO);
        intentFilter.addAction(EnviarImagenesIntentService.TERMINO_FALTARON_IMGS);
        intentFilter.addAction(EnviarImagenesIntentService.TERMINO_ENVIO_OKAY);

        // Creamos instancias de nuestro Broadcast
        EnviarDatosReceiver receiver = new EnviarDatosReceiver();

        // Registramos el receiver con los filtros
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, intentFilter);
    }

    private boolean validarCampos(){
        if(edtDireccion.getText().toString().trim().isEmpty()){
            edtDireccion.setError("Campo requerido");
            edtDireccion.requestFocus();

            return false;
        }

        if(edtDescripcion.getText().toString().trim().isEmpty()){
            edtDescripcion.setError("Debe escribir una descripcion de su solicitud");
            edtDescripcion.requestFocus();

            return false;
        }

        if(markerPosicion == null){
            Toast.makeText(this, "Debe agregar la posicion donde se efecturará el trabajo, en el mapa", Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;
    }

    /**
     *  Chequea cuales permisos faltan y los pide
     * @return false si hay algun permiso faltante
     */
    private boolean pedirPermisosFaltantes(){
        boolean todosConsedidos = true;
        ArrayList<String> permisosFaltantes = new ArrayList<>();

        boolean permisoCamera = ( ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED);

        boolean permisoEscrituraSD = ( ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED);

        boolean permisoLecturaSD = ( ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED);

        boolean permisoCoarseLocation = ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED);

        boolean permisoFineLocation = ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED);


        if(!permisoCamera){
            todosConsedidos = false;
            permisosFaltantes.add(Manifest.permission.CAMERA);
        }

        if(!permisoEscrituraSD){
            todosConsedidos = false;
            permisosFaltantes.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if(!permisoLecturaSD){
            todosConsedidos = false;
            permisosFaltantes.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if(!permisoCoarseLocation){
            todosConsedidos = false;
            permisosFaltantes.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if(!permisoFineLocation){
            todosConsedidos = false;
            permisosFaltantes.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if(!todosConsedidos) {
            String[] permisos = new String[permisosFaltantes.size()];
            permisos = permisosFaltantes.toArray(permisos);

            ActivityCompat.requestPermissions(this, permisos, COD_PERMISOS);
        }

        return todosConsedidos;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        mapa = googleMap;
    }

    private class EnviarDatosReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null && intent.getAction() != null){
                switch (intent.getAction()){
                    case EnviarImagenesIntentService.ACTUALIZAR_PROGESO:
                        int imgEnviadas = intent.getIntExtra(EnviarImagenesIntentService.NUM_IMGS_ENVIADAS, 0);

                        txtLayoutProgeso.setText("Enviado imagenes.. ("+imgEnviadas+"/"+adapteFotos.getItemCount()+")");

                        break;
                    case EnviarImagenesIntentService.TERMINO_ENVIO_OKAY:
                        layoutProgreso.setVisibility(View.GONE);
                        progressPublicar.setVisibility(View.GONE);
                        btnPublicar.setVisibility(View.VISIBLE);

                        AlertDialog dialogFinOkay = new AlertDialog.Builder(CrearPublicacionActivity.this)
                                .setTitle("Su solicitud se realizó correctamente")
                                .setCancelable(false)
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).create();

                        if(!isFinishing())
                            dialogFinOkay.show();

                        break;
                    case EnviarImagenesIntentService.TERMINO_FALTARON_IMGS:
                        layoutProgreso.setVisibility(View.GONE);
                        progressPublicar.setVisibility(View.GONE);
                        btnPublicar.setVisibility(View.VISIBLE);

                        int numImgFalt = intent.getIntExtra(EnviarImagenesIntentService.NUM_IMGS_FALTANTES, 0);
                        final int codSolicitud = intent.getIntExtra(EnviarImagenesIntentService.EXTRA_COD_SOLICITUD, 0);

                        AlertDialog dialogFinImgsFaltantes = new AlertDialog.Builder(CrearPublicacionActivity.this)
                                .setTitle("Se envió su solicitud, pero no se pudieron enviar "+numImgFalt+" imagenes")
                                .setCancelable(false)
                                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Lanzamos el servicio de enviar imagenes
                                        Intent intentEnviarImgs = new Intent(CrearPublicacionActivity.this, EnviarImagenesIntentService.class);
                                        intentEnviarImgs.setAction(EnviarImagenesIntentService.ACTION_ENVIAR_IMGS);
                                        intentEnviarImgs.putExtra(EnviarImagenesIntentService.EXTRA_COD_SOLICITUD, codSolicitud);
                                        intentEnviarImgs.putExtra(EnviarImagenesIntentService.EXTRA_IMAGENES, adapteFotos.getImagenes());
                                        startService(intentEnviarImgs);
                                    }
                                })
                                .setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).create();

                        if(!isFinishing())
                            dialogFinImgsFaltantes.show();

                        break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_publicar:

                if(validarCampos()){
                    progressPublicar.setVisibility(View.VISIBLE);
                    btnPublicar.setVisibility(View.GONE);
                    layoutProgreso.setVisibility(View.VISIBLE);
                    txtLayoutProgeso.setText("Procesando...");

                    LatLng latLng = markerPosicion.getPosition();

                }


                break;

            case R.id.btn_agregar_foto:
                if(pedirPermisosFaltantes()) {
                    AlertDialog.Builder builderAialogoAjusteHora = new AlertDialog.Builder(this);
                    builderAialogoAjusteHora.setMessage("Elija de donde tomar la imagen")
                            .setPositiveButton("CAMARA", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    try {
                                        cameraPhoto = new FotoDeCamara(getApplicationContext());

                                        Intent intentFoto = cameraPhoto.takePhotoIntent();
                                        startActivityForResult(intentFoto, INTENT_CAMARA);
                                        //cameraPhoto.addToGallery();
                                    }catch (IOException e){
                                        Toast.makeText(CrearPublicacionActivity.this, "No se pudo iniciar la camara.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).setNegativeButton("GALERIA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            galleryPhoto = new GalleryPhoto(getApplicationContext());

                            Intent intentGaleria = galleryPhoto.openGalleryIntent();
                            startActivityForResult(intentGaleria, INTENT_GALERIA);
                        }
                    });
                    // creamos el dialogo
                    AlertDialog dialogo = builderAialogoAjusteHora.create();
                    dialogo.show();// mostramos el dialogo
                }

                break;
            case R.id.btn_cambiar_ubicacion:
                if(pedirPermisosFaltantes()) {
                    Intent intentSetUbicacion = new Intent(this, DefinirPosicionActivity.class);
                    startActivityForResult(intentSetUbicacion, COD_SET_UBICACION);
                }
                break;
            case R.id.btn_agregar_servicio:

                if(!edtNuevoServicio.getText().toString().trim().isEmpty()){

                    String servicio = edtNuevoServicio.getText().toString();

                    adapterServicios.agregarTexto(servicio);

                    txtLabelServicios.setVisibility(View.GONE);
                }else{
                    edtNuevoServicio.setError("Debe escribir els ervicio");
                    edtNuevoServicio.requestFocus();
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case INTENT_CAMARA:
                    try{
                        txtLabelFotos.setVisibility(View.GONE);

                        adapteFotos.addImagen(new Imagen(adapteFotos.getItemCount()+1, cameraPhoto.getPhotoPath()));
                    }catch (NullPointerException e){
                        e.printStackTrace();
                        Toast.makeText(this, "Error al cargar la foto, intente de nuevo.", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case INTENT_GALERIA:
                    galleryPhoto.setPhotoUri(data.getData());

                    // Si el peso es 0 Kb es porque la imagen no existe
                    if(FotoDeCamara.pesoKBytesFile(galleryPhoto.getPath()) != 0){
                        txtLabelFotos.setVisibility(View.GONE);

                        adapteFotos.addImagen(new Imagen(adapteFotos.getItemCount()+1, galleryPhoto.getPath()));
                    }else{
                        Toast.makeText(this, "La imagen que eligió no es valida.", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case COD_SET_UBICACION:
                    double latitud = data.getDoubleExtra(DefinirPosicionActivity.EXTRA_LATITUDE, 0.0);
                    double longitude = data.getDoubleExtra(DefinirPosicionActivity.EXTRA_LONGITUDE, 0.0);
                    String direccion = data.getStringExtra(DefinirPosicionActivity.EXTRA_DIRECCION);

                    if(latitud != 0.0 && longitude != 0.0 && direccion != null && mapa != null){
                        mapa.moveCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(latitud, longitude), 16));
                        markerPosicion = mapa.addMarker( new MarkerOptions().position( new LatLng(latitud, longitude) ));
                    }

                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
