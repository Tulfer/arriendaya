package tulio.ing.arriendaya.actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.basedatos.DBManager;
import tulio.ing.arriendaya.modelos.DatosLogin;
import tulio.ing.arriendaya.modelos.Mensaje;
import tulio.ing.arriendaya.servicioweb.ServicioWeb;
import tulio.ing.arriendaya.servicioweb.ServicioWebUtils;

public class LoginActivity extends AppCompatActivity {

    private EditText edtUsuario, edtPass;
    private ServicioWeb servicioWeb;
    private DBManager dbManager;
    private String TAG = "ActividadLogin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dbManager = new DBManager(this);

        edtUsuario = findViewById(R.id.edt_usuario);
        edtPass = findViewById(R.id.edt_pass);

        servicioWeb = ServicioWebUtils.getServicioWeb();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    public void entrar(View btn){
        if(validarCampos()){
            Call<Mensaje> mensajeCall = servicioWeb.login(
                    "login",
                    edtUsuario.getText().toString(),
                    edtPass.getText().toString()
            );

            mensajeCall.enqueue(new Callback<Mensaje>() {
                @Override
                public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                    if(response.isSuccessful()){
                        Mensaje mensaje = response.body();

                        if(mensaje != null){
                            Log.v(TAG, "retorno de login "+mensaje.getMensaje());

                            switch (mensaje.getMensaje()){
                                case "ok_login":

                                    DatosLogin datosLogin = mensaje.getUsuario();

                                    if(datosLogin != null){
                                        dbManager.insertarDatosLogin(datosLogin);

                                        finish();
                                    }

                                    break;
                                default:
                                    Toast.makeText(LoginActivity.this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
                                    break;
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<Mensaje> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Revise su conexion e intente de nuevo", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    public void clickIrRegistro(View btn){
        Intent intent = new Intent(this, RegistroActivity.class);
        startActivity(intent);
    }

    public boolean validarCampos(){
        if(edtUsuario.getText().toString().trim().isEmpty()){
            edtUsuario.setError("Digite su usuario");
            edtUsuario.requestFocus();
            return false;
        }

        if(edtPass.getText().toString().trim().isEmpty()){
            edtPass.setError("Digite su contraseña");
            edtPass.requestFocus();
            return false;
        }

        return true;
    }
}
