package tulio.ing.arriendaya.actividades;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.basedatos.DBManager;
import tulio.ing.arriendaya.modelos.DatosLogin;
import tulio.ing.arriendaya.modelos.Mensaje;
import tulio.ing.arriendaya.servicioweb.ServicioWeb;
import tulio.ing.arriendaya.servicioweb.ServicioWebUtils;

public class RegistroActivity extends AppCompatActivity {

    private EditText edtUsuario, edtPass, edtPassConfirmar, edtNombres, edtApellidos,
                    edtTelefono, edtEmail, edtDireccion;
    private Button btnEnviarRegistro;
    private Spinner spnGeneros;
    private ServicioWeb servicioWeb;
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        setToolbar();

        dbManager = new DBManager(this);

        edtUsuario = findViewById(R.id.edt_usuario_registro);
        edtPass = findViewById(R.id.edt_pass_registro);
        edtPassConfirmar = findViewById(R.id.edt_pass_confirmar_registro);
        edtNombres = findViewById(R.id.edt_nombres_registro);
        edtApellidos = findViewById(R.id.edt_apellidos_registro);
        edtTelefono = findViewById(R.id.edt_telefono_registro);
        edtEmail = findViewById(R.id.edt_email_registro);

        edtDireccion = findViewById(R.id.edt_direccion_registro);
        btnEnviarRegistro = findViewById(R.id.btn_enviar_registro);
        spnGeneros = findViewById(R.id.spn_generos_registro);

        servicioWeb = ServicioWebUtils.getServicioWeb();
    }

    public void clickRegistrarme(View btn){
        if(validarCampos()){
            Call<Mensaje> mensajeCall = servicioWeb.registro(
                    "registro",
                    edtUsuario.getText().toString(),
                    edtPass.getText().toString(),
                    edtNombres.getText().toString(),
                    edtApellidos.getText().toString(),
                    edtTelefono.getText().toString(),
                    edtDireccion.getText().toString(),
                    edtEmail.getText().toString(),
                    spnGeneros.getSelectedItem().toString()
            );

            mensajeCall.enqueue(new Callback<Mensaje>() {
                @Override
                public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                    if(response.isSuccessful()){
                        Mensaje mensaje = response.body();

                        if(mensaje != null){
                            if( mensaje.getMensaje().equals("okay") ){
                                DatosLogin datosLogin = mensaje.getUsuario();

                                if(datosLogin != null){

                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegistroActivity.this);
                                    builder.setTitle("Registro exitoso")
                                            .setMessage("Revise email, le enviamos un correo para activar su cuenta. Recuerde revisar el Spam")
                                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            })
                                            .setCancelable(false);
                                    builder.create().show();

                                }
                            }else{
                                Toast.makeText(RegistroActivity.this, "Error en los datos", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<Mensaje> call, Throwable t) {
                    Toast.makeText(RegistroActivity.this, "Revise su conexion e intente de nuevo", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar_registro);

        if(toolbar != null){
            setSupportActionBar(toolbar);

            if(getSupportActionBar() != null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle("Registro");
            }
        }
    }

    private boolean validarCampos(){
        if(edtUsuario.getText().toString().trim().isEmpty()){
            edtUsuario.setError("Campo requirido");
            edtUsuario.requestFocus();

            return false;
        }

        if(edtPass.getText().toString().trim().isEmpty()){
            edtPass.setError("Campo requirido");
            edtPass.requestFocus();

            return false;
        }

        if(edtPassConfirmar.getText().toString().trim().isEmpty()){
            edtPassConfirmar.setError("Campo requirido");
            edtPassConfirmar.requestFocus();

            return false;
        }

        if( !edtPass.getText().toString().equals( edtPassConfirmar.getText().toString() ) ){
            Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            edtPass.requestFocus();

            return false;
        }

        if( edtPass.getText().toString().length() < 6 ){
            edtPass.setError("De tener minimo 6 caracteres");
            edtPass.requestFocus();

            return false;
        }

        if(edtNombres.getText().toString().trim().isEmpty()){
            edtNombres.setError("Campo requirido");
            edtNombres.requestFocus();

            return false;
        }

        if(edtApellidos.getText().toString().trim().isEmpty()){
            edtApellidos.setError("Campo requirido");
            edtApellidos.requestFocus();

            return false;
        }

        if(edtTelefono.getText().toString().trim().isEmpty()){
            edtTelefono.setError("Campo requirido");
            edtTelefono.requestFocus();

            return false;
        }

        if(edtEmail.getText().toString().trim().isEmpty()){
            edtEmail.setError("Campo requirido");
            edtEmail.requestFocus();

            return false;
        }

        // Patrón para validar el email
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        // El email a validar
        String email = edtEmail.getText().toString().trim();

        Matcher mather = pattern.matcher(email);

        if(!mather.find()){
            edtEmail.requestFocus();
            edtEmail.setError("Email no valido");

            return false;
        }

        if(edtDireccion.getText().toString().trim().isEmpty()){
            edtDireccion.setError("Campo requirido");
            edtDireccion.requestFocus();

            return false;
        }

        return true;
    }

}
