package tulio.ing.arriendaya.actividades;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.adapters.TextoAdapter;
import tulio.ing.arriendaya.fragments.ImagenFragment;
import tulio.ing.arriendaya.modelos.Arriendo;
import tulio.ing.arriendaya.servicioweb.ServicioWeb;
import tulio.ing.arriendaya.servicioweb.ServicioWebUtils;

public class DetallesArriendoActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener{

    public static final String ARG_ID_ARRIENTO = "id_arriendo";

    private ViewPager viewPagerImagenes;
    private ArrayList<String> imagenes;
    private MapFragment mapFrament;
    private GoogleMap miMapa;
    private RecyclerView recyclerServicio;
    private ArrayList<String> servicios;
    private TextoAdapter serviciosAdapter;
    private CircleImageView imgPersona;
    private TextView txtDireccion;
    private TextView txtNombrePersona;
    private LinearLayout layoutContenido, layoutReintentar;
    private ProgressBar progress;
    private int idArriendo;
    private TextView txtAnuncio;
    private Button btnReintentar;
    private ServicioWeb servicioWeb;
    private TextView txtDescripcion;
    private MiFragmentPager miFragmentPager;
    private String TAG = "ActividadDetalles";
    private Arriendo arriendo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_arriendo);
        setToolbar();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            idArriendo = bundle.getInt(ARG_ID_ARRIENTO);
        }

        layoutReintentar = findViewById(R.id.layout_reintentar_detalles);
        txtAnuncio = findViewById(R.id.txt_anuncio_reintentar);
        btnReintentar = findViewById(R.id.btn_reintantar_detalles);
        layoutContenido = findViewById(R.id.layout_contenido);

        imgPersona = findViewById(R.id.img_persona);
        txtNombrePersona = findViewById(R.id.txt_nombre_persona);
        txtDireccion = findViewById(R.id.txt_direccion);
        txtDescripcion = findViewById(R.id.txt_descripcion);
        progress = findViewById(R.id.progress_detalles);

        imagenes = new ArrayList<>();
        viewPagerImagenes = findViewById(R.id.pager_imagenes);

        miFragmentPager = new MiFragmentPager(getSupportFragmentManager());
        miFragmentPager.setUrlIms(imagenes);

        viewPagerImagenes.setAdapter(miFragmentPager);

        recyclerServicio = findViewById(R.id.recycler_servicios);

        RecyclerView.LayoutManager lmServicios = new LinearLayoutManager(this);
        recyclerServicio.setLayoutManager(lmServicios);
        recyclerServicio.setNestedScrollingEnabled(false);

        servicios = new ArrayList<String>();
        serviciosAdapter = new TextoAdapter(servicios);

        recyclerServicio.setAdapter(serviciosAdapter);

        mapFrament = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_mapa_detalles);
        mapFrament.getMapAsync(this);

        servicioWeb = ServicioWebUtils.getServicioWeb();

        refreshDatos();
    }

    private void setToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar_detalles_arriento);

        if(toolbar != null){
            setSupportActionBar(toolbar);

            if(getSupportActionBar() != null){
                getSupportActionBar().setTitle("");
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        miMapa = googleMap;
        miMapa.setOnMapClickListener(this);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(arriendo != null) {
            Intent intent = new Intent(this, MapaActivity.class);
            intent.putExtra("latitud", arriendo.getLatitud());
            intent.putExtra("longitud", arriendo.getLongitud());
            startActivity(intent);
        }
    }

    private class MiFragmentPager extends FragmentPagerAdapter {
        private ArrayList<String> urlIms = new ArrayList<>();

        public MiFragmentPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ImagenFragment.newInstance( urlIms.get(position) );
        }

        @Override
        public int getCount() {
            return urlIms.size();
        }

        public void setUrlIms(ArrayList<String> urlIms){
            this.urlIms = urlIms;
        }
    }


    private void refreshDatos(){
        progress.setVisibility(View.VISIBLE);
        layoutContenido.setVisibility(View.GONE);
        layoutReintentar.setVisibility(View.GONE);

        Log.v(TAG, "idArriendo "+idArriendo);

        Call<Arriendo> arriendoCall = servicioWeb.getArriendo(
                "getArriendo",
                idArriendo + ""
        );

        arriendoCall.enqueue(new Callback<Arriendo>() {
            @Override
            public void onResponse(Call<Arriendo> call, Response<Arriendo> response) {
                if(response.isSuccessful()){

                    Arriendo arri = response.body();

                    if(arri!= null){
                        arriendo = arri;
                        establecerDatos(arriendo);

                        layoutReintentar.setVisibility(View.GONE);
                        layoutContenido.setVisibility(View.VISIBLE);
                    }

                }else{
                    txtAnuncio.setText("Error en el servidor");
                    layoutReintentar.setVisibility(View.VISIBLE);
                }

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Arriendo> call, Throwable t) {
                progress.setVisibility(View.GONE);
                txtAnuncio.setText("Conexion insuficiente");
                layoutReintentar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void establecerDatos(Arriendo arriendo){
        txtNombrePersona.setText( arriendo.getNombrePersona() );
        txtDireccion.setText( arriendo.getDireccion() );
        txtDescripcion.setText( arriendo.getDescripcion() );

        if(arriendo.getServicios() != null){
            serviciosAdapter.setTextos( arriendo.getServicios() );
        }

        if(miMapa != null){
            if(arriendo.getLatitud() != 0.0 && arriendo.getLongitud() != 0.0){
                miMapa.addMarker( new MarkerOptions().position( new LatLng(arriendo.getLatitud(), arriendo.getLongitud() ) ) );
                miMapa.moveCamera( CameraUpdateFactory.newLatLngZoom( new LatLng(arriendo.getLatitud(), arriendo.getLongitud() ), 16) );
            }
        }

        Glide.with(this)
                .load( arriendo.getUrlImagenPersona() )
                .into(imgPersona);

        imagenes = arriendo.getUrlsImagenes();

        if(imagenes != null){
            miFragmentPager.setUrlIms( imagenes );
            miFragmentPager.notifyDataSetChanged();
        }


    }
}
