package tulio.ing.arriendaya.actividades;

import android.content.ClipData;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.fragments.FragmentNovedades;
import tulio.ing.arriendaya.fragments.MiPerfilFragment;
import tulio.ing.arriendaya.material.bottomnavigation.BottomNavigationItemView;

public class ActividadPrincipal extends AppCompatActivity {

    private FrameLayout tabsContent;
    private BottomNavigationItemView BNovedades;
    private FragmentNovedades fragmentNovedades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        tabsContent = findViewById(android.R.id.tabcontent);

        fragmentNovedades = new FragmentNovedades();

        cambiarFragmetns(0);// ponemos el que está por defecto : novedades
    }

    public void cambiarFragmetns(int idItem){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fragmentNovedades;// por defecto es novedades

        switch (idItem){
            case R.id.action_buscar:

                break;
            case R.id.action_cuenta:
                fragment = new MiPerfilFragment();
                break;
            case R.id.action_novedades:
                fragment = fragmentNovedades;
                break;
        }


        FragmentTransaction ft = fm.beginTransaction().replace(R.id.Lnovedades, fragment);
        ft.commit();
    }
}
