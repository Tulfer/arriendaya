package tulio.ing.arriendaya.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.modelos.Arriendo;
import tulio.ing.arriendaya.modelos.NovedadItem;

public class NovedadesAdapter extends RecyclerView.Adapter<NovedadesAdapter.NovedadesViewHolder> {



    public interface ListenerClick {
        void clickDetalles(Arriendo novedad, int posicion);
    }

    private ArrayList<Arriendo> novedades;
    private ListenerClick miListener;
    private Context contexto;

    public NovedadesAdapter(ArrayList<Arriendo> novedades, ListenerClick miListener, Context contexto) {
        this.novedades = novedades;
        this.miListener = miListener;
        this.contexto = contexto;
    }

    public class NovedadesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final Button btnDetalles;
        private ImageView imgPersona, imgNovedad;
        private TextView txtNombrePersona, txtDireccion, txtDescripcion, txtPrecio;

        public NovedadesViewHolder(View itemView) {
            super(itemView);

            imgPersona = itemView.findViewById(R.id.item_img_persona);
            txtNombrePersona = itemView.findViewById(R.id.item_nombre_persona);
            txtDireccion = itemView.findViewById(R.id.item_direccion_novedad);
            txtDescripcion = itemView.findViewById(R.id.item_descripcion_noveedad);
            imgNovedad = itemView.findViewById(R.id.item_img_novedad);
            btnDetalles = itemView.findViewById(R.id.item_btn_detalles_novedad);
            txtPrecio = itemView.findViewById(R.id.item_txt_precio);

            btnDetalles.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if(miListener != null) {
                switch (view.getId()) {
                    case R.id.item_btn_detalles_novedad:
                        miListener.clickDetalles( novedades.get( getAdapterPosition() ), getAdapterPosition() );
                        break;
                }
            }
        }
    }

    @Override
    public NovedadesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from( parent.getContext() ).inflate(R.layout.item_novedad, parent, false);

        return new NovedadesViewHolder(item);
    }

    @Override
    public void onBindViewHolder(NovedadesViewHolder holder, int position) {
        Arriendo novedad = novedades.get(position);

        try {
            Glide.with(contexto)
                    .load(novedad.getUrlsImagenes().get(0))
                    .into(holder.imgNovedad);
        }catch (Exception e){
            Glide.with(contexto)
                    .load(R.drawable.imagen_en_blanco)
                    .into(holder.imgNovedad);
        }

        Glide.with(contexto)
                .load(novedad.getUrlImagenPersona())
                .into(holder.imgPersona);

        holder.txtNombrePersona.setText( novedad.getNombrePersona() );
        holder.txtDescripcion.setText( novedad.getDescripcion() );
        holder.txtDireccion.setText( novedad.getDireccion() );
        holder.txtPrecio.setText( "$" + novedad.getPrecio() );
    }

    @Override
    public int getItemCount() {
        return novedades.size();
    }

    public void setNovedades(ArrayList<Arriendo> novedades) {
        this.novedades = novedades;
        notifyDataSetChanged();
    }
}
