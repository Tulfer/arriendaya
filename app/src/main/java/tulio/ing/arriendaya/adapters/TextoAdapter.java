package tulio.ing.arriendaya.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tulio.ing.arriendaya.R;

public class TextoAdapter extends RecyclerView.Adapter<TextoAdapter.TextViewHolder>{
    private ArrayList<String> textos;

    public TextoAdapter(ArrayList<String> textos) {
        this.textos = textos;
    }

    @Override
    public TextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from( parent.getContext() ).inflate(R.layout.item_texto, parent, false);

        return new TextViewHolder(item);
    }

    @Override
    public void onBindViewHolder(TextViewHolder holder, int position) {
        String texto = textos.get(position);

        holder.txtTexto.setText( texto == null ? "" : texto );
    }

    @Override
    public int getItemCount() {
        return textos.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTexto;

        public TextViewHolder(View itemView) {
            super(itemView);

            txtTexto = itemView.findViewById(R.id.item_txt_texto);
        }
    }

    public void setTextos(ArrayList<String> textos) {
        this.textos = textos;
        notifyDataSetChanged();
    }

    public void agregarTexto(String texto){
        textos.add(texto);
        notifyDataSetChanged();
    }

    public String getTextosPorComas(){
        String cadena = "";

        for(String texto : textos){
            cadena += texto + ", ";
        }

        // quitamos ultima coma y espacio
        if(cadena.length() > 2) {
            cadena = cadena.substring(0, cadena.length() - 2);
        }

        return cadena;
    }
}
