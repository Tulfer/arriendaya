package tulio.ing.arriendaya.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import tulio.ing.arriendaya.R;
import tulio.ing.arriendaya.modelos.Imagen;

public class ImagenAdapter extends RecyclerView.Adapter<ImagenAdapter.ImagenViewHolder> {

    private ArrayList<Imagen> imagenes;
    private Context contexto;

    public ImagenAdapter(ArrayList<Imagen> imagenes, Context contexto) {
        this.imagenes = imagenes;
        this.contexto = contexto;
    }

    public class ImagenViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;

        public ImagenViewHolder(View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.item_img);
        }
    }

    @NonNull
    @Override
    public ImagenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_imagen, parent, false);

        return new ImagenViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagenViewHolder holder, int position) {
        Imagen imagen = imagenes.get(position);

        Glide.with(contexto)
                .load( imagen.getDireccion() )
                .into( holder.img );
    }

    @Override
    public int getItemCount() {
        return imagenes.size();
    }

    public void addImagen(Imagen imagen){
        imagenes.add(imagen);
        notifyDataSetChanged();
    }

    public ArrayList<Imagen> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<Imagen> imagen){
        this.imagenes.clear();
        this.imagenes.addAll(imagen);
        notifyDataSetChanged();
    }
}
