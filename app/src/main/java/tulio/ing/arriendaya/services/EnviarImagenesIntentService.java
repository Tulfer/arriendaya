package tulio.ing.arriendaya.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import tulio.ing.arriendaya.modelos.Imagen;
import tulio.ing.arriendaya.utilidades.EnviarImagen;


public class EnviarImagenesIntentService extends IntentService {

    public static final String ACTION_ENVIAR_IMGS = "enviar_imgs";

    public static final String EXTRA_IMAGENES = "extra_imgs";
    public static final String EXTRA_COD_SOLICITUD = "extra_cod";

    public static final String ACTUALIZAR_PROGESO = "progreso";
    public static final String TERMINO_ENVIO_OKAY = "temino";
    public static final String TERMINO_FALTARON_IMGS = "faltaron";

    public static final String NUM_IMGS_FALTANTES = "num_faltantes";
    public static final String NUM_IMGS_ENVIADAS = "num_enviadas";

    private String TAG = "ServicioEnviarImgs";

    private ArrayList<Imagen> imagenesPorEnviar;
    private ArrayList<Imagen> imagenesNoEnviadas =  new ArrayList<>();
    private String codSolicitud;

    public EnviarImagenesIntentService() {
        super("EnviarImagenesIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_ENVIAR_IMGS.equals(action)) {
                imagenesPorEnviar = (ArrayList<Imagen>) intent.getSerializableExtra(EXTRA_IMAGENES);
                codSolicitud = intent.getStringExtra(EXTRA_COD_SOLICITUD);

                int intentos = 0;
                do{
                    enviarImagenes();

                    intentos++;// Se intentara enviar todas las imagenes tres veces
                }while (imagenesNoEnviadas.size() > 0 && intentos < 3);

                if(imagenesNoEnviadas.size() > 0){
                    Intent iFaltanImgs = new Intent(TERMINO_FALTARON_IMGS);
                    iFaltanImgs.putExtra(NUM_IMGS_FALTANTES, imagenesNoEnviadas.size());
                    iFaltanImgs.putExtra(EXTRA_COD_SOLICITUD, codSolicitud);
                }else {
                    // Se enviaron todas las imagenes
                    Intent iTerminoOkay = new Intent(TERMINO_ENVIO_OKAY);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(iTerminoOkay);
                }
            }
        }
    }

    private void enviarImagenes(){
        // Se construye la notificacion
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "michaneldeimg")
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setContentTitle("Enviando imagenes")
                .setContentText("Procesado...");

        int contadorImgEnviada = 0, contadorImgNoEnviada = 0;

        int cantidadImagenesPorEnviar = imagenesPorEnviar.size();

        for (Imagen imagen : imagenesPorEnviar) {
            try {
                File fileImagenOriginal = new File(imagen.getDireccion());

                File fileImagenComprimida;
                try {
                    // Comprimimos la imagen en este hilo
                    fileImagenComprimida= new Compressor(this)
                            .compressToFile(fileImagenOriginal);

                }catch (OutOfMemoryError e){
                    // Si ocurre un error con abuso de memoria al comprimir enviamos la imagen original sin comprimir
                    fileImagenComprimida = fileImagenOriginal;
                }

                // tratamos en eviar la imagen y verificamos que se envio y las contamos
                if (EnviarImagen.subirArchivo(fileImagenComprimida.getAbsolutePath(), codSolicitud+"-"+imagen.getId())) {
                    contadorImgEnviada++;
                } else {
                    imagenesNoEnviadas.add(imagen);

                    contadorImgNoEnviada++;
                }

                // actualizamos el progreso de la notificacion
                builder.setProgress(cantidadImagenesPorEnviar, contadorImgEnviada, false);
                builder.setContentText("Enviando... (" + contadorImgEnviada + "/" + cantidadImagenesPorEnviar + ")");
                // ponemos en primer plano la notificacion y le asignamos un id
                startForeground(123, builder.build());

                Intent iActualizarProgreso = new Intent(ACTUALIZAR_PROGESO);
                iActualizarProgreso.putExtra(NUM_IMGS_ENVIADAS, contadorImgEnviada);
                LocalBroadcastManager.getInstance(this).sendBroadcast(iActualizarProgreso);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "Error " + e.getMessage());
            } catch (NullPointerException e) {
                e.printStackTrace();
                Log.e(TAG, "File de la imagen original nula");
            }
        }
    }
}
