package tulio.ing.arriendaya.servicioweb;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tulio.ing.arriendaya.utilidades.Constantes;

public class ServicioWebUtils {

    public static ServicioWeb getServicioWeb(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create( ServicioWeb.class );
    }
}
