package tulio.ing.arriendaya.servicioweb;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import tulio.ing.arriendaya.modelos.Arriendo;
import tulio.ing.arriendaya.modelos.Mensaje;
import tulio.ing.arriendaya.modelos.NovedadItem;
import tulio.ing.arriendaya.utilidades.Constantes;

public interface ServicioWeb {

    @FormUrlEncoded
    @POST(Constantes.URL_HOME)
    Call<ArrayList<Arriendo>> getNovedades(@Field("accion") String accion);

    @FormUrlEncoded
    @POST(Constantes.URL_HOME)
    Call<Arriendo> getArriendo(@Field("accion") String accion,
                               @Field("idArriendo") String idArriendo);

    @FormUrlEncoded
    @POST(Constantes.URL_HOME)
    Call<Mensaje> login(@Field("accion") String accion,
                        @Field("usuario") String usuario,
                        @Field("pass") String pass);

    @FormUrlEncoded
    @POST(Constantes.URL_HOME)
    Call<Mensaje> registro(@Field("accion") String accion,
                           @Field("usuario") String usuario,
                           @Field("pass") String pass,
                           @Field("nombre") String nombre,
                           @Field("apellidos") String apellidos,
                           @Field("telefono") String telefono,
                           @Field("direccion") String direccion,
                           @Field("email") String email,
                           @Field("sexo") String sexo);

    @FormUrlEncoded
    @POST(Constantes.URL_HOME)
    Call<Mensaje> publicarArriendo(@Field("accion") String accion,
                                   @Field("descripcion") String descripcion,
                                   @Field("precio") String precio,
                                   @Field("idUsuario") String idUsuario,
                                   @Field("direccion") String direccion,
                                   @Field("latitud") String latitud,
                                   @Field("longitud") String longitud,
                                   @Field("servicios") String servicios);// los servicios van separados por _

    @FormUrlEncoded
    @POST(Constantes.URL_HOME)
    Call<Mensaje> solicitarArriendo(@Field("accion") String accion,
                                    @Field("idArriendo") int idArriendo,
                                    @Field("idPersona") int idPersina);

}
